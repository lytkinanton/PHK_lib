/*
 * main.h
 *
 *  Created on: Sep 2, 2013
 *      Author: lan
 */

#ifndef MAIN_H_
#define MAIN_H_

#define BANNER ("===_ACD EMITATION SERVER_===")

#define CTRL_PACK 13

#define OW_EVENT1 12
#define OW_EVENT2 13
#define IO_EVENT 14

#define MAXPENDING 1    /* Max connection requests */
#define BUFFSIZE  13

/* Shift values for RTC_STAT register */
#define DAY_BITS_OFF    17
#define HOUR_BITS_OFF   12
#define MIN_BITS_OFF    6
#define SEC_BITS_OFF    0

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef char s8;
typedef short s16;
typedef int s32;

#define DIRECTION_A 0
#define DIRECTION_B 1

int save2Bmp(char* filename, u8 *pImage, u16 height, u16 width);
int get_cur_ms();


#endif /* MAIN_H_ */
