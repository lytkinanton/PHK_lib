#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include "serialib.h"

#define NEWLINE "\r\n"
class io_currency_ctrl
{
#define START_SESSION "HUEA1\r\n"
#define STOP_SESSION "HUEA0\r\n"
#define GET_CURRENCY "HUEB\r\n"
#define GET_STATUS "HUEC\r\n"
#define PUSH_COINER1 "HUEF"
#define PUSH_COINER2 "HUEG"
#define MOST_MESSAGE_LEN 14

#define HOPPER1 1
#define HOPPER2 1
private:
    char * port_name;
    serialib port;

public:
    io_currency_ctrl(char * port)
    {
        port_name = new char[strlen(port)];
        strcpy(port_name, port);
        return;
    }

    ~io_currency_ctrl()
    {
        delete port_name;
        port.Close();
    }

    char init()
    {
        return port.Open(port_name, 115200);
    }

    signed char start_session()
    {
        char buffer[MOST_MESSAGE_LEN];
        int len = 0;
        memset(buffer, 0, MOST_MESSAGE_LEN);
        if (port.WriteString(START_SESSION))
        {
            printf("Write error :("NEWLINE);
            return -1;
        }
        len = port.Read(buffer, MOST_MESSAGE_LEN, 1000);

        if (len >= 0)
            if (strcmp(START_SESSION, buffer) == 0)
            {
                printf("Session started"NEWLINE);
                return 0;
            } else {
                return -1;
            }

    }
    signed char stop_session()
    {
        char buffer[MOST_MESSAGE_LEN];
        int len = 0;
        memset(buffer, 0, MOST_MESSAGE_LEN);
        if (port.WriteString(STOP_SESSION))
        {
            printf("Write error :("NEWLINE);
            return -1;
        }
        len = port.Read(buffer, MOST_MESSAGE_LEN, 1000);

        if (len >= 0)
            if (strcmp(STOP_SESSION, buffer) == 0)
            {
                printf("Session stopped"NEWLINE);
                return 0;
            } else {
                return -1;
            }
    }

    int get_currency()
    {
        char buffer[MOST_MESSAGE_LEN];
        int len = 0;
        memset(buffer, 0, MOST_MESSAGE_LEN);
        if (port.WriteString(GET_CURRENCY))
        {
            printf("Write error :("NEWLINE);
            return -1;
        }
        if ( (len = port.Read(buffer, 14, 100)) )
        {
            // no errors
         //   printf("RCV len %d: [%s]...", len, buffer);
            return parse_for_value(buffer);

        }
    }

    int parse_for_value(char * buf)
    {
        if ((buf[0] == 'H') &&
            (buf[1] == 'U') &&
            (buf[2] == 'E')) {

            return  (unsigned int)strtol(&buf[4], NULL, 16);
        }
        return -1;
    }

    int push_coiner (unsigned char coiner, unsigned char value)
    {
        char buffer[MOST_MESSAGE_LEN];
        int len = 0;

        if ( value > 20 ){
            printf("Too much coins!"NEWLINE);
            return -1;
        }
        char cmd[MOST_MESSAGE_LEN];
        sprintf(cmd, "%s%X\r\n", (coiner == 1)?PUSH_COINER1:PUSH_COINER2, value);
        memset(buffer, 0, MOST_MESSAGE_LEN);

        if (port.WriteString(cmd))
        {
            printf("Write error :("NEWLINE);
            return -1;
        }
        if ( (len = port.Read(buffer, 14, 100)) )
        {
            // no errors
            printf("COINER%d PUSHING %d"NEWLINE,coiner, parse_for_value(buffer));
            return parse_for_value(buffer);

        }
    }

    char get_status()
    {
        char buffer[MOST_MESSAGE_LEN];
        int len = 0;
        memset(buffer, 0, MOST_MESSAGE_LEN);
        if (port.WriteString(GET_STATUS))
        {
            printf("Write error :("NEWLINE);
            return -1;
        }
        if ( (len = port.Read(buffer, 14, 100)) )
        {
            // no errors
         //   printf("RCV len %d: [%s]...", len, buffer);
            return parse_for_value(buffer);

        }
    }
};

int main(int argc, char **argv)
{
    io_currency_ctrl * io = new io_currency_ctrl("COM4");

    if (!io->init() )
    {
        printf("Port opened OK"NEWLINE);
    } else {
        printf("Port open FAILED!"NEWLINE);
        return -1;
    }
    printf("Device status %d"NEWLINE, io->get_status());

    if (io->start_session()) {
        printf("Start session FAILED!"NEWLINE);
        return -1;
    }
    sleep(20);
    if (io->stop_session()) {
        printf("Stop session FAILED!"NEWLINE);
    }

    printf("Overall currency %d"NEWLINE, io->get_currency());

    io->push_coiner(HOPPER2, 4);

    int tmo = 10;
    while ((io->push_coiner(HOPPER2, 0) > 0) && (--tmo))
    {
        sleep(1);
    }

    printf("Device status %d"NEWLINE, io->get_status());

    return 0;

}

